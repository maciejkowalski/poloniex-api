package poloniex

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"sync"
	"time"

	"gopkg.in/beatgammit/turnpike.v2"
)

type (
	//Poloniex describes the API
	Poloniex struct {
		Key          string
		Secret       string
		ws           *turnpike.Client
		subscribedTo map[string]bool
		debug        bool
		nonce        int64
		mutex        sync.Mutex
	}
)

const (
	// PUBLICURI is the address of the public API on Poloniex
	PUBLICURI = "https://poloniex.com/public"
	// PRIVATEURI is the address of the public API on Poloniex
	PRIVATEURI = "https://poloniex.com/tradingApi"
)

// Init sets up the API instance correctly
func (p *Poloniex) Init() {
	p.nonce = time.Now().UnixNano()
	p.mutex = sync.Mutex{}
}

func (p *Poloniex) InitWS() {
	if p.ws != nil {
		return
	}
	t := &tls.Config{InsecureSkipVerify: true}
	c, err := turnpike.NewWebsocketClient(turnpike.JSON, "wss://api.poloniex.com", t)
	if err != nil {
		log.Fatalln(err)
	}
	_, err = c.JoinRealm("realm1", nil)
	if err != nil {
		log.Fatalln(err)
	}
	p.ws = c
	p.subscribedTo = map[string]bool{}

}

func (p *Poloniex) isSubscribed(code string) bool {
	ok := p.subscribedTo[code]
	return ok
}

func (p *Poloniex) Debug() {
	p.debug = true
}

func (p *Poloniex) GetNonce() string {
	p.mutex.Lock()
	defer p.mutex.Unlock()
	p.nonce++
	return fmt.Sprintf("%d", p.nonce)
}

func New(configfile string) *Poloniex {
	p := &Poloniex{}
	b, err := ioutil.ReadFile(configfile)
	if err != nil {
		log.Fatalln(err)
	}
	err = json.Unmarshal(b, p)
	if err != nil {
		log.Fatalln(err)
	}
	p.Init()
	return p
}

func PreviousPeriod(period string, num ...int) int64 {
	switch {
	case period == "month":
		n := 1
		if len(num) > 0 {
			n = num[0]
		}
		return time.Now().AddDate(0, -n, 0).Unix()
	case period == "week":
		n := 1
		if len(num) > 0 {
			n = num[0]
		}
		return time.Now().AddDate(0, 0, -n).Unix()
	case period == "day":
		n := 1
		if len(num) > 0 {
			n = num[0]
		}
		return time.Now().AddDate(0, 0, -n).Unix()
	case period == "hour":
		n := 3600 * time.Second
		if len(num) > 0 {
			n = time.Duration(num[0]) * n
		}
		return time.Now().Add(-n).Unix()
	case period == "minute":
		n := 60 * time.Second
		if len(num) > 0 {
			n = time.Duration(num[0]) * n
		}
		return time.Now().Add(-n).Unix()
	}
	return time.Now().Unix()
}
